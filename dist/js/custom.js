(function($){
	var openMobileMenu = function () {
	  var body = $('body');
	  if (body.hasClass('open')) {
			closeMobileMenu();
		} else {
			body.addClass('open');
		}
	}
	var closeMobileMenu = function () {
		$('body').removeClass('open');
	}

	/* MOBILE MENU */
	var mobileMenu = $('.js-mobile-menu');
	$(document).on('click', '.js-mobile-button', function () {
		openMobileMenu();
		return false;
	});

	$(document).off('click.mobileMenu').on('click.mobileMenu', function (e) {
		var t = $(e.target);
		closeMobileMenu(mobileMenu);
		if (t.closest('.js-mobile-menu').length < 1 && t.closest('.js-mobile-button').length < 1) {
				closeMobileMenu(mobileMenu);
		}
	});

	$('.js-side-menu').on('click', function(){
		$(this).parent().toggleClass('open');
	});
	$('.show-pass').on('click', function(){
		$(this).toggleClass('show');
	});

	$(document).ready(function() {
		$('.menu').onePageNav({
			currentClass: 'current',
			changeHash: true,
			scrollSpeed: 750,
			scrollThreshold: 0.5,
			filter: ':not(.external)',
			easing: 'swing'
		});
	});

	$('#chatModal').on('shown.bs.modal', function () {
      $(".modal-backdrop").remove();
   	})


   	$('.sponsored').slick({
	  dots: true,
	  arrows: false,
	  infinite: false,
	  speed: 300,
	  slidesToShow: 1,
	  centerMode: false,
	  variableWidth: true
	});

	$('.js-expand').on('click', function(){
		$(this).parents('.sponsored-list').toggleClass('open');
		return false;
	});

	$('.js-filter').on('click', function(){
		$('.js-category').toggleClass('show');
	});
	$('.js-filter2').on('click', function(){
		$('.js-category2').toggleClass('show');
	});

	// $('#viewTable').change(function() {

	//     if ($('#viewTable').prop('checked')) {
	//       window.location.href = "/lounge.html";
	//     }
	// 	});
	// $('#listTable').change(function() {

	//     if ($('#listTable').prop('checked')) {
	//       window.location.href = "/lounge2.html";
	//     }
	// 	});
		
		document.querySelector('#viewTable').addEventListener('change', function(){
			if(this.checked){
				setTimeout(function(){window.location.href="lounge.html"},250);
			 }
			 else{
				 setTimeout(function(){window.location.href="lounge2.html"},250);
			 }  
		 });
		 
	// const $dropdown = $(".dropdown");
	// const $dropdownToggle = $(".dropdown-toggle");
	// const $dropdownMenu = $(".dropdown-menu");
	// const showClass = "show";
	// $(window).on("load resize", function() {
	// 	if (this.matchMedia("(min-width: 768px)").matches) {
	// 		$dropdown.hover(
	// 			function() {
	// 				const $this = $(this);
	// 				$this.addClass(showClass);
	// 				$this.find($dropdownToggle).attr("aria-expanded", "true");
	// 				$this.find($dropdownMenu).addClass(showClass);
	// 			},
	// 			function() {
	// 				const $this = $(this);
	// 				$this.removeClass(showClass);
	// 				$this.find($dropdownToggle).attr("aria-expanded", "false");
	// 				$this.find($dropdownMenu).removeClass(showClass);
	// 			}
	// 		);
	// 	} else {
	// 		$dropdown.off("mouseenter mouseleave");
	// 	}
	// });
})(jQuery);